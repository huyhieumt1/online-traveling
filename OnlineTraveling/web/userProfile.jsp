<%-- 
    Document   : userProfile
    Created on : Sep 16, 2022, 10:09:18 PM
    Author     : ACER
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
        <link href='https://fonts.googleapis.com/css?family=Inter' rel='stylesheet'>
        <link rel="stylesheet" href="CSS/banner.css">
        <link rel="stylesheet" href="CSS/userProfile.css">
        <style>
            body {
                font-family: 'Inter';
                font-size: 22px;
            }
        </style>
        <title>Profile</title>
    </head>
    <body>
        <div class="container-md">
            <div class="header">
                <div class="row">
                    <!--logo-->
                    <div class="col-md-2">
                        <a href="home">
                            <img src="images/Logo.png" alt="alt"/>
                        </a>
                    </div>

                    <!--home-->
                    <div class="col-md-1 redir">
                        <a href="home">Trang chủ</a>
                    </div>
                    <!--lưu trú-->
                    <div class="col-md-1 redir">
                        <a href="#">Lưu trú</a>
                    </div>
                    <!--thuê xe-->
                    <div class="col-md-1 redir">
                        <a href="#">Thuê xe</a>
                    </div>
                    <!--địa điểm tham quan-->
                    <div class="col-md-2 redir">
                        <a href="#">Địa điểm tham quan</a>
                    </div>
                    <!--cách đoạn-->
                    <div class="col-md-1">
                        <div></div>
                    </div>

                    <c:if test="${sessionScope.account == null}">
                        <!--Đăng nhập doanh nghiệp-->
                        <div class="col-md-2 redir">
                            <a href="#">Đăng nhập doanh nghiệp</a>
                        </div>

                        <!--đăng nhập-->
                        <div class="col-md-1 redir">
                            <a href="login">Đăng nhập</a>
                        </div>
                        <!--đăng ký-->
                        <div class="col-md-1 registerBtn">
                            <a href="register">Đăng ký</a>
                        </div>
                    </c:if>
                    <c:if test="${sessionScope.account != null}">
                        <!--place holder-->
                        <div class="col-md-1">
                        </div>



                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-2 profilePhoto">
                                    <a href="userprofile?userID=${sessionScope.account.userID}">
                                        <img src="images/user.png" alt="alt"/>
                                    </a> 
                                </div>
                                <div class="col-md-6 username">
                                    <a href="userprofile?userID=${sessionScope.account.userID}">${sessionScope.account.userName}</a>
                                </div>
                                <!-- 
                                <div class="col-md-3 firstName">
                                    <a href="#">${sessionScope.account.firstName}</a>
                                </div>
                                -->
                                <div class="col-md-3 logout">
                                    <a href="logout">Logout</a>
                                </div>
                            </div>
                        </div>
                    </c:if>
                </div>
            </div>

            <c:if test="${sessionScope.account.role == 1 || requestScope.currentID == sessionScope.account.userID}">
                <div class="row">
                    <div class="col-md-3 left-board">
                        <div class="option" id="currentOption">
                            <a href="userprofile?userID=${requestScope.currentID}">Thông tin cá nhân</a>
                        </div>
                        <c:if test="${requestScope.currentID == sessionScope.account.userID}">
                            <div class="option">
                                <a href="#">Cập nhật thông tin</a>
                            </div>
                            <div class="option" id="lastOption">
                                <a href="#">Đổi mật khẩu</a>
                            </div>
                        </c:if>
                    </div>

                    <div class="col-md-8 right-board">
                        <p id="personalInfo">Thông tin cá nhân</p>
                        <!--First name-->
                        <hr>
                        <div class="row infoRow">
                            <div class="col-md-3 infoTitle">
                                <P>Tên:</P>
                            </div>
                            <div class="col-md-9">
                                <P>${requestScope.currentUser.firstName}</P>
                            </div>
                        </div>

                        <!--Last name-->
                        <hr>
                        <div class="row infoRow">
                            <div class="col-md-3 infoTitle">
                                <P>Họ:</P>
                            </div>
                            <div class="col-md-9">
                                <P>${requestScope.currentUser.lastName}</P>
                            </div>
                        </div>

                        <!--Gender-->
                        <hr>
                        <div class="row infoRow">
                            <div class="col-md-3 infoTitle">
                                <P>Giới tính:</P>
                            </div>
                            <div class="col-md-9">
                                <P>
                                    <c:if test="${requestScope.currentUser.gender == false}">Female</c:if>
                                    <c:if test="${requestScope.currentUser.gender == true}">Male</c:if>
                                    </P>
                                </div>
                            </div>

                            <!--DOB-->
                            <hr>
                            <div class="row infoRow">
                                <div class="col-md-3 infoTitle">
                                    <P>Ngày sinh:</P>
                                </div>
                                <div class="col-md-9">
                                    <P>
                                    <c:if test="${requestScope.currentUser.dob == null}">...</c:if>
                                    <c:if test="${requestScope.currentUser.dob != null}">${requestScope.currentUser.dob}</c:if>
                                    </P>
                                </div>
                            </div>

                            <!--Address-->
                            <hr>
                            <div class="row infoRow">
                                <div class="col-md-3 infoTitle">
                                    <P>Địa chỉ:</P>
                                </div>
                                <div class="col-md-9">
                                    <P>
                                    <c:if test="${requestScope.currentUser.address == null}">...</c:if>
                                    <c:if test="${requestScope.currentUser.address != null}">${requestScope.currentUser.address}</c:if>
                                    </P>
                                </div>
                            </div>

                            <!--Phone Number-->
                            <hr>
                            <div class="row infoRow">
                                <div class="col-md-3 infoTitle">
                                    <P>Số Điện Thoại:</P>
                                </div>
                                <div class="col-md-9">
                                    <P>
                                    <c:if test="${requestScope.currentUser.phone == null}">...</c:if>
                                    <c:if test="${requestScope.currentUser.phone != null}">${requestScope.currentUser.phone}</c:if>
                                    </P>
                                </div>
                            </div>

                            <!--Email-->
                            <hr>
                            <div class="row infoRow">
                                <div class="col-md-3 infoTitle">
                                    <P>Email:</P>
                                </div>
                                <div class="col-md-9">
                                    <P>
                                    <c:if test="${requestScope.currentUser.email == null}">...</c:if>
                                    <c:if test="${requestScope.currentUser.email != null}">${requestScope.currentUser.email}</c:if>
                                    </P>
                                </div>
                            </div>
                        </div>
                    </div>


            </c:if>

            <c:if test="${sessionScope.account.role != 1 && requestScope.currentID != sessionScope.account.userID}">
                <h1>You are not allowed to see the information of this page</h1>
                <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
            </c:if>

            <!--Space holder-->

            <br/><br/><br/>
            <!--Footer-->
            <div class="footer">
                <div class="row">
                    <!--Logo
                    <div class="col-md-1"><img src="images/Logo.png" alt="alt"/></div>
                    -->
                    <!--Contact information-->
                    <div class="col-md-6 contactInfo">
                        <p id="infoTitle">Địa chỉ và thông tin liên hệ</p>
                        <div class="informationCard">
                            <p>Đại Học FPT Hà Nội, Km29 Đường Cao Tốc 08, Thạch Hoà, Thạch Thất, Hà Nội</p>
                            <p>Tel: 0945066089</p>
                            <p>Email: CuongNDHE163098@fpt.edu.vn</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>
