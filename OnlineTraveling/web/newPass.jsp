<%-- 
    Document   : newPass
    Created on : Sep 16, 2022, 7:24:00 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form action="newPassword" method="post">         
            <label for="password">New Password: </label>
            <input type="password" id="newPass" name="newPass"><br/>
            <label for="confirm">Confirm Password: </label>
            <input type="password" id="confirm" name="confirm"><br/>
            <p id="message"></p>
            <input type="button" onclick="checkPassword()" value="SUBMIT" id="send"/>
        </form>
    </body>
      <script>
            function checkPassword() {
                let password = document.getElementById("newPass").value;
                let confirm = document.getElementById("confirm").value;
                console.log(" Password:", newPass, '\n', "Confirm Password:", confirm);
                let message = document.getElementById("message");

                if (password.length != 0) {
                    if (password == confirm) {
                       document.getElementById("send").type = 'submit';
                    } else {
                        message.textContent = "Password don't match";
                    }
                } else {
                    alert("Password can't be empty!");
                    message.textContent = "";
                }
            }
        </script>
</html>
