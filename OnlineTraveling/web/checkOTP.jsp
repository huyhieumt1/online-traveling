<%-- 
    Document   : checkOTP
    Created on : Sep 16, 2022, 7:19:06 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h2>${requestScope.message}</h2>
        <form action="checkOTP" method="post">
            <label for="otp">Enter you OTP: </label>
            <input type="text" name="otp"><br/>
            <input type="submit" value="SUBMIT">
        </form>
    </body>
</html>
