<%-- 
    Document   : Register.jsp
    Created on : Sep 15, 2022, 9:08:21 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Chào mừng đến với Book'sStay</h1>
        <h1>Hãy tạo tài khoản Book'sStay của bạn</h1>
        <span>${requestScope.mess}</span>
        <form action="register" method="post">
            <div><h3>Địa chỉ email</h3></div>
            <input type="email" name="email" placeholder="Nhập email của bạn..."><br>
            <div><h3>Tên người dùng của bạn</h3></div>
            <input  type="text" placeholder="Nhập tên người dùng của bạn...." name="username">
            <div><h3>Ten cua ban</h3></div>
            <input  type="text" placeholder="Nhập ten của bạn...." name="firstName"><br>
            <div><h3>Ho cua ban</h3></div>
            <input  type="text" placeholder="Nhập ho của bạn...." name="lastName"><br>
            <div><h3>Mật khẩu:</h3></div>            
            <input type="password" name="password" placeholder="Nhập mật khẩu..." id="newPass"><br>
            <div><h3>Nhập lại mật khẩu:</h3></div>
            <input type="password" name="confirm_password" placeholder="Nhập mật khẩu..." id="confirm"><br>
            <p id="message"></p>
            <input type="button" value="Đăng ký" onclick="checkPassword()" id="send">
        </form>
    </body>
    <script>
        function checkPassword() {
            let password = document.getElementById("newPass").value;
            let confirm = document.getElementById("confirm").value;
            console.log(" Password:", newPass, '\n', "Confirm Password:", confirm);
            let message = document.getElementById("message");

            if (password.length != 0) {
                if (password == confirm) {
                    document.getElementById("send").type = 'submit';
                } else {
                    message.textContent = "Password don't match";
                }
            } else {
                alert("Password can't be empty!");
                message.textContent = "";
            }
        }
    </script>

</html>
