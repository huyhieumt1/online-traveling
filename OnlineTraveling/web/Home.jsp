<%-- 
    Document   : home.jsp
    Created on : Sep 13, 2022, 9:26:17 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
        <link href='https://fonts.googleapis.com/css?family=Inter' rel='stylesheet'>
        <link rel="stylesheet" href="CSS/banner.css">
        <link rel="stylesheet" href="CSS/homepage.css">
        <style>
            body {
                font-family: 'Inter';
                font-size: 22px;
            }
        </style>
        <title>Book'nStay</title>
    </head>
    <body>
        <div class="container-md">
            <div class="header">
                <div class="row">
                    <!--logo-->
                    <div class="col-md-2">
                        <a href="home">
                            <img src="images/Logo.png" alt="alt"/>
                        </a>
                    </div>

                    <!--home-->
                    <div class="col-md-1 redir">
                        <a href="home">Trang chủ</a>
                    </div>
                    <!--lưu trú-->
                    <div class="col-md-1 redir">
                        <a href="#">Lưu trú</a>
                    </div>
                    <!--thuê xe-->
                    <div class="col-md-1 redir">
                        <a href="#">Thuê xe</a>
                    </div>
                    <!--địa điểm tham quan-->
                    <div class="col-md-2 redir">
                        <a href="#">Địa điểm tham quan</a>
                    </div>
                    <!--cách đoạn-->
                    <div class="col-md-1">
                        <div></div>
                    </div>

                    <c:if test="${sessionScope.account == null}">
                        <!--Đăng nhập doanh nghiệp-->
                        <div class="col-md-2 redir">
                            <a href="#">Đăng nhập doanh nghiệp</a>
                        </div>

                        <!--đăng nhập-->
                        <div class="col-md-1 redir">
                            <a href="login">Đăng nhập</a>
                        </div>
                        <!--đăng ký-->
                        <div class="col-md-1 registerBtn">
                            <a href="register">Đăng ký</a>
                        </div>
                    </c:if>
                    <c:if test="${sessionScope.account != null}">
                        <!--place holder-->
                        <div class="col-md-1">
                        </div>



                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-2 profilePhoto">
                                    <a href="userprofile?userID=${sessionScope.account.userID}">
                                        <img src="images/user.png" alt="alt"/>
                                    </a> 
                                </div>
                                <div class="col-md-6 username">
                                    <a href="userprofile?userID=${sessionScope.account.userID}">${sessionScope.account.userName}</a>
                                </div>
                                <!-- 
                                <div class="col-md-3 firstName">
                                    <a href="#">${sessionScope.account.firstName}</a>
                                </div>
                                -->
                                <div class="col-md-3 logout">
                                    <a href="logout">Logout</a>
                                </div>
                            </div>
                        </div>
                    </c:if>
                </div>
            </div>

            <!--Banner-->
            <div class="banner">
                <div class="row">
                    <img src="images/Banner.png" alt="alt"/>
                    <p class="title">Book'nStay</p>
                    <p class="belowTitle">Let's start getting you one (or more) unforgettable journey of your life.</p>
                </div>
            </div>


            <!--Space holder
            <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>-->
            <br/><br/><br/><br/><br/>

            <!--Dashboard-->
            <div class="row">
                <div class="col-md-8"></div>
                <div class="col-md-1"></div>
                <c:if test="${sessionScope.account.role == 1}">
                    <div class="dashboard col-md-3">
                        <p>Dashboard</p>
                        <p>System admin</p>
                        <a href="#">Room type list</a>
                        <br/>
                        <a href="#">User List</a>
                    </div>
                </c:if>
            </div>

            <br/><br/><br/><br/><br/>


            <!--Footer-->
            <div class="footer">
                <div class="row">
                    <!--Logo
                    <div class="col-md-1"><img src="images/Logo.png" alt="alt"/></div>
                    -->
                    <!--Contact information-->
                    <div class="col-md-6 contactInfo">
                        <p id="infoTitle">Địa chỉ và thông tin liên hệ</p>
                        <div class="informationCard">
                            <p>Đại Học FPT Hà Nội, Km29 Đường Cao Tốc 08, Thạch Hoà, Thạch Thất, Hà Nội</p>
                            <p>Tel: 0945066089</p>
                            <p>Email: CuongNDHE163098@fpt.edu.vn</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
