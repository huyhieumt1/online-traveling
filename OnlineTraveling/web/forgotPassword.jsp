<%-- 
    Document   : forgotPassword
    Created on : Sep 16, 2022, 11:03:39 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Forgot Password Page</title>
    </head>
    <body>
        <h3>${requestScope.notFound}</h3>
        <form action="forgot" method="post">
            <label for="email">Enter your Email: </label>
            <input type="email" name="email">
            <input type="submit" value="Send">
        </form>
    </body>
</html>
