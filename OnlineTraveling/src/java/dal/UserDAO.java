/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import models.User;

/**
 *
 * @author Admin
 */
public class UserDAO extends DBContext {

    //insert user 
    public void insertUser(String email, String username, String password, String lastName, String firstName) {
        try {
            String sql = "INSERT INTO [dbo].[User]\n"
                    + "           ([username]\n"
                    + "           ,[password]\n"
                    + "           ,[lastName]\n"
                    + "           ,[firstName]\n"
                    + "           ,[role]\n"
                    + "           ,[status]\n"
                    + "           ,[email])\n"
                    + "     VALUES (?,?,?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1,username);
            ps.setString(2, password);
            ps.setString(3, lastName);
            ps.setString(4, firstName);
            ps.setInt(5, 2);
            ps.setBoolean(6, true);
            ps.setString(7, email);           
            ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    //check username
    public User checkValidUserName(String username) {
        try {
            String sql = "select * from User where username=?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return new User(rs.getString("username"),
                        rs.getString("password"),
                        rs.getString("lastName"),
                        rs.getString("firstName"),
                        rs.getBoolean("gender"),
                        rs.getDate("dob"),
                        rs.getString("address"),
                        rs.getInt("role"),
                        rs.getBoolean("status"),
                        rs.getString("phone"),
                        rs.getString("email")
                );
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }
    //check user account

    public User getUser(String email, String password) {

        try {
            String sql = "select * from [User] where email = ? and password = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, email);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return new User(rs.getInt("userID"),
                        rs.getString("username"),
                        rs.getString("password"),
                        rs.getString("lastName"),
                        rs.getString("firstName"),
                        rs.getBoolean("gender"),
                        rs.getDate("dob"),
                        rs.getString("address"),
                        rs.getInt("role"),
                        rs.getBoolean("status"),
                        rs.getString("phone"),
                        rs.getString("email")
                );
            }
        } catch (SQLException ex) {
        }
        return null;
    }
    
    // Get User by ID 
    public User getUserById(int userID) {

        try {
            String sql = "select * from [User] where userID = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, userID);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return new User(rs.getInt("userID"),
                        rs.getString("username"),
                        rs.getString("password"),
                        rs.getString("lastName"),
                        rs.getString("firstName"),
                        rs.getBoolean("gender"),
                        rs.getDate("dob"),
                        rs.getString("address"),
                        rs.getInt("role"),
                        rs.getBoolean("status"),
                        rs.getString("phone"),
                        rs.getString("email")
                );
            }
        } catch (SQLException ex) {
        }
        return null;
    }
    // check user password 

    public boolean checkPass(String pass) {
        String sql = "select * from [User] where password = ?";
        boolean check = false;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, pass);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                check = true;
            }
        } catch (SQLException e) {
        }
        return check;
    }

    //update password
    public void updatePassword(String email, String pass) {
        String sql = "UPDATE [dbo].[User]\n"
                + "   SET [password] = ?  \n"
                + " WHERE [email] = ? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, pass);
            st.setString(2, email);
            st.executeUpdate();
        } catch (SQLException e) {
        }
    }

    //check user email
    public boolean checkEmail(String email) {
        String sql = "select * from [User] where email = ?";
        boolean check = false;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                check = true;
            }
        } catch (SQLException e) {
        }
        return check;
    }
    public static void main(String[] args) {
        UserDAO ur = new UserDAO();
        
        System.out.println(ur.getUserById(1).getUserName());
    }
}
