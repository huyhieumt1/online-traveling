CREATE DATABASE [Online-Traveling]
GO
Use [Online-Traveling]
GO
CREATE TABLE [User](
    [userID] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](100) NULL,
	[password] [varchar](50) NULL,
	[lastName] [nvarchar](100) NULL,
	[firstName] [nvarchar](100) NULL,
	[gender] [bit] NULL,
	[dob] DATE NULL CHECK (YEAR(GETDATE()) - YEAR(dob) >=18),
	[address] [nvarchar](255) NULL,
	[role] [int] NULL,
	[status] [bit] NULL,
	[phone] [varchar](50) NULL,
	[email] [varchar](50) NULL,
)
GO
CREATE TABLE [Business](
     [busiID] int IDENTITY (1,1) NOT NULL,
	 [username] nvarchar(150) NOT NULL,
	 [busiName] nvarchar(150) NOT NULL,
	 [email] nvarchar(50) NOT NULL,
	 [phonenumber] INT NOT NULL,
	 [status] BIT NOT NULL,
	 [password] varchar(150) NOT NULL
)
GO 
CREATE TABLE [RoomType](
   [roomTypeID] INT IDENTITY (1,1) NOT NULL,
   [roomTypeName] varchar(150) NOT NULL,
   [description] varchar(150) NOT NULL
)
